Title: gcc48_on_CentOS
Date: 2014-11-12
Category: lab
Tags: centos, gcc
Summary: CentOS 6.5に入ってるGCCは4.4.7の古いバージョンなので，新しいやつを入れます．
Authors: Jie211

CentOS 6.5に入ってるGCCは4.4.7の古いバージョンなので，新しいやつを入れます．

{% codeblock lang:bash %}
    #!bash
    root ~ # cat /etc/redhat-release
    CentOS release 6.5 (Final)
    root ~ # gcc --version
    gcc (GCC) 4.4.7 20120313 (Red Hat 4.4.7-11)
    Copyright (C) 2010 Free Software Foundation, Inc.
    This is free software; see the source for copying conditions.  There is NO
    warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.))))

RedHat Developer Toolset v2.0のレポジトリをyumに入れてインストール

    #!bash
    root ~ # wget http://people.centos.org/tru/devtools-2/devtools-2.repo -O /etc/yum.repos.d/devtools-2.repo
    root ~ # yum install devtoolset-2-gcc devtoolset-2-binutils
    root ~ # yum install devtoolset-2-gcc-c++ devtoolset-2-gcc-gfortran

リンクを貼る

    #!bash
    ln -s /opt/rh/devtoolset-2/root/usr/bin/gcc /usr/local/bin/gcc48
    ln -s /opt/rh/devtoolset-2/root/usr/bin/g++ /usr/local/bin/g++

以上．

