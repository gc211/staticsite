Title: WireLurker
Date: 2014-11-07
Category: mac
Tags: mac
Summary: WireLurker
Authors: Jie211

ニュースにより中国からWireLurkerというmalicious codeが356,000超えのmacに感染したらしい．

念のためチェックする
チェック用のgithubからスクリプトがあった

    #!bash
    curl -O https://raw.githubusercontent.com/PaloAltoNetworks-BD/WireLurkerDetector/master/WireLurkerDetectorOSX.py

    python WireLurkerDetectorOSX.py
