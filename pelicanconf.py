#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Jie211'
SITENAME = 'Rain Coming'
SITEURL = 'http://www.jie211.me'

PATH = 'content'

TIMEZONE = 'Asia/Tokyo'

DEFAULT_LANG = 'jp'

THEME = "./pelican-themes/clean"

COLOR_SCHEME_CSS = 'monokai.css'

GITHUB_URL= 'https://github.com/Jie211'
TWITTER_URL= 'https://twitter.com/jie211'
WEIBO_URL= 'http://weibo.com/534230012'
INSTAGRAM_URL= 'https://www.instagram.com/jie211'

GOOGLE_ANALYTICS = "UA-72003524-1"
DISQUS_SITENAME= "raincome"

PLUGIN_PATHS= ['pelican-plugins']
MD_EXTENSIONS = ['linkify', 'del_ins', 'fenced_code', 'codehilite(css_class=highlight)', 'tables']
PLUGINS= ['sitemap', 'render_math']
SITEMAP = {
        "format": "xml",
        "priorities": {
            "articles": 0.7,
            "indexes": 0.5,
            "pages": 0.3,
            },
        "changefreqs": {
            "articles": "monthly",
            "indexes": "daily",
            "pages": "monthly",
            }
}
MATH_JAX = {'color':'blue'}
STATIC_PATHS = [
        'extra/robots.txt',
        'extra/favicon.ico'
        ]
EXTRA_PATH_METADATA={
        'extra/robots.txt': {'path': 'robots.txt'},
        'extra/favicon.ico': {'path': 'favicon.ico'}
}
# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
DATE_FORMATS = {
        'en': '%a, %d %b %Y',
        'jp': '%Y-%m-%d(%a)',
}
# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('twitter', 'https://twitter.com/jie211'),
        ('github', 'https://github.com/Jie211'),
        ('weibo', 'http://weibo.com/534230012'),
        ('instagram', 'https://www.instagram.com/jie211'))

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
